''' @file fibonaccigenerator.py 
Created by Andrew R Noble, this script generates fibonacci numbers
at the user-specified index'''

# @mainpage
#
# @section intro_sec Introduction
# This program produces fibonacci numbers at specfied fibonacci numbers. 

# @author Andrew R. Noble
# @date   April 24, 2020

import sys

# @section fib_sec Fibonacci Generator Function
#
#  This function returns the fibonacci number at the index that is passed to it.
#
# @param idx The index of the desired fibonacci number. 

def fib(idx):
    print('Calculating Fibonacci number at index n = {}.'.format(idx))
    if int(idx) == 0 or int(idx) == 1: #need to hardcode the first two indicies
        print('The Fibonacci number at that index is {}.'.format(idx))
    else:
        lastlast = 0 #represents the value two indicies ago
        last = 1 #value at previous index
        for count in range(2,idx+1): #range normally does not include the last number, but we need it to, hence +1 (also skip, 0,1)
            newfib = lastlast + last #calculate fib number for this iteration
            lastlast = last #reassign the n-1 value from last iteration to the n-2 value
            last = newfib #reassign the new value to n-1 for next iter
        print('The Fibonacci number at index {}'.format(idx) + ' is {}'.format(newfib))
        
'''This code only executes if fibonaccigeneartor.py is the main python file being run. This is done
so that the fib() function can be used by other scripts without executing the below code. 
'''


# @section userIO_sec User Interaction Code
#
# This code interacts with the user to accept fibonacci indexes and reject invalid input.
#
# Asks the user for an integer fibonacci index and then calls fib() with that
# provided index. Non-integer inputs are caught and the user is re-prompted. The program
# can be ended using a special input "exit".
#        
    
if __name__ == '__main__':
    try:
        fib(int(input('Enter the index of the desired Fibonacci number.')))
    except ValueError:
        print('That is not a valid integer index, try again.')
    while True:
        choice = input('Enter another index or "exit" to exit the program.')
        try:   
            if choice == 'exit':
                sys.exit()
            else:
                fib(int(choice))
        except ValueError:       
            print('That is not a valid integer entry.') #catch non-integer entries
