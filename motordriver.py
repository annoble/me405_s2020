''' @file motordriver.py
Brief doc for motordriver.py

Detailed doc for motordriver.py 

@author Andrew R. Noble
@date April 24, 2020

@package motordriver
The motor module implements functionality for controlling a DC motor. 

@author Andrew R. Noble
@date April 24, 2020'''

import pyb


class Motor:
    '''This motor driver class is used to control a DC motor. It takes the desired CPU GPIO pin
    objects and timer object and prepares them for the operation of a DC motor including stopping,
    starting, and changing the speed fo the motor.
    @author Andrew R. Noble
    @date April 24, 2020'''
        
    def __init__(self, EN_pin, IN1_pin, IN2_pin, timer, debug=False):
        '''
        Creates a motor driver object by initializing GPIO pins and turning the motor off for safety.   
        @param EN_pin  A pyb.Pin object to use as the enable pin.
        @param IN1_pin A pyb.Pin object to use as the input to half bridge 1.
        @param IN2_pin A pyb.Pin object to use as the input to half bridge 2.
        @param timer   A pyb.Timer object to use for PWM generation on IN1_pin and IN2_pin.
        '''
        
        # create instance pin and timer attributes
        self.EN_pin = EN_pin
        self.IN1_pin = IN1_pin
        self.IN2_pin = IN2_pin
        self.timer = timer
        EN_pin.init(pyb.Pin.OUT_PP)
        IN1_pin.init(pyb.Pin.OUT_PP)
        IN2_pin.init(pyb.Pin.OUT_PP)
        EN_pin.low() #turn the motor off for safety
        self.timer_ch1 = self.timer.channel(1, pyb.Timer.PWM, pin=self.IN1_pin) # create two timer channels to facilitate forward and backward rotation
        self.timer_ch2 = self.timer.channel(2, pyb.Timer.PWM, pin=self.IN2_pin)
        self.debug = debug
        if self.debug:
            print('Initializing motor: ' + str(self))
        
    
    def enable(self):
        '''Enables the motor by raising the user-selected enabler pin.'''
        
        self.EN_pin.high() #raise the enable pin
        self.timer_ch1.pulse_width_percent(0)
        self.timer_ch2.pulse_width_percent(0) 
        if self.debug:
            print('Enabling Motor: ' + str(self))
    
    def disable(self):
        '''Disables the motor by lowering the user-selected enabler pin.'''
        
        self.EN_pin.low() #lower the enable pin
        self.timer_ch1.pulse_width_percent(0)
        self.timer_ch2.pulse_width_percent(0) 
        if self.debug:
            print('Disabling Motor: ' + str(self))
            
    
    def set_duty(self, duty):
        '''This method sets the duty cycle to be sent to the motor at the given level.
        Positive values cause effort in the one direction, negative values in the
        opposite direction.   
        @param duty A signed number holding the duty cycle of the PWM signal
        sent to the motor.'''
        
        if type(duty) is not int and type(duty) is not float or duty < -100.0 or duty > 100.0:
            print('Invalid percentage duty cycle entry, it must be a signed number between -100 and 100')
        
        elif duty > 0:
            
            #clear previous rotation settings
            self.timer_ch1.pulse_width_percent(0) 
            self.IN1_pin.low()
            self.IN2_pin.low()
            
            #set duty
            self.timer_ch2.pulse_width_percent(duty)
            
        elif duty < 0:
            
            self.timer_ch2.pulse_width_percent(0) 
            self.IN1_pin.low()
            self.IN2_pin.low()
            
            self.timer_ch1.pulse_width_percent(abs(duty))

        if self.debug:
            print('Setting Percent Duty Cycle on Motor: ' + str(self))          
        

if __name__ == '__main__':
    '''This block of code is a test case for the motor driver.'''
   
    # Creating the pin objects used for interfacing with the motor driver.
    enbpin  = pyb.Pin(pyb.Pin.cpu.A10)
    IN1pin  = pyb.Pin(pyb.Pin.cpu.B4)
    IN2pin  = pyb.Pin(pyb.Pin.cpu.B5)
   
    # Creating the timer object used for PWM generation
    tim     = pyb.Timer(3, freq = 20000)
   
    # Creating a motordriver object and passing in the desired pins and timer
    moe     = Motor(enbpin,IN1pin,IN2pin,tim,debug=True)
   
    # Enable the motor driver
    moe.enable()
   
    #set duty cycle of the motor
    moe.set_duty(20)