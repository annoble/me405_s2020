''' @file IMUdriver.py
Brief doc for IMUdriver.py

Detailed doc for IMUdriver.py 

@author Andrew R. Noble
@date May 17, 2020

@package IMUdriver
The IMU (inertial measurement unit) module implements functionality for controlling the 9-axis
Bosch BNO055 IMU. Specifically, this module includes the IMu class which allows the user to get angular velocity and angular position data.
Note: the BNO055 default I2C address is 40 (0x28).

@author Andrew R. Noble
@date May 23, 2020'''

import pyb
import ustruct
import utime

class IMU:
    '''This IMU class is used to interface with the BNO055 IMU. It allows the user to:
    - enable, disable or set the mode of the IMU
    - check the calibration status of the IMU
    - get orientation of the IMU as 3 euler angles (returned as a tuple)
    - get the three angular velocities and return (returned as a tuple). '''
    
    def __init__(self, bus):
        '''Creates an I2C object on the desired bus (bus 1 = CPU pins B6 and B7 on the nucleo board).'''
        self.i2c = pyb.I2C(bus)
        
    def enable(self):
        '''Enables the IMU by initializing the I2C bus to master mode.'''
        self.i2c.init(pyb.I2C.MASTER)
    
    def disable(self):
        '''Disables the IMU by deinitializing the I2C bus.'''
        self.i2c.disable()
    
    def mode(self, mode_choice = None):
        '''Allows the user to change the mode of the BNO055 chip.
        If no mode is provided, the current mode name is returned (and printed).
		@param mode_choice The selected IMU mode, in the same all-caps format that is used in the BNO055 documentation.'''
		
        
        mode_dict = {'CONFIGMODE'  : 0b0000,
                     'ACCONLY'     : 0b0001,
                     'MAGONLY'     : 0b0010,
                     'GYROONLY'    : 0b0011,
                     'ACCMAG'      : 0b0100,
                     'ACCGYRO'     : 0b0101,
                     'MAGGYRO'     : 0b0110,
                     'AMG'         : 0b0111,
                     'IMU'         : 0b1000,
                     'COMPASS'     : 0b1001,
                     'M4G'         : 0b1010,
                     'NDOF_FMC_OFF': 0b1011,
                     'NDOF'        : 0b1100}
        
        if mode_choice == None:
            data = self.i2c.mem_read(1, 40, 0x3D) #read the value of the OPR_MODE register
            code = data[0] & 0b00001111 # reduce the data down to just the first 4 bits, corresponding to the mode code we want
            
            for name, cde in mode_dict.items():
                if code == cde:
                    print(name)
                    return name
        else:
            try:
                data = self.i2c.mem_read(1, 40, 0x3D) #read the current mode
                data_new = (data[0] & 0b11110000) + mode_dict[mode_choice] # replace current mode selection with new selected one
                self.i2c.mem_write(data_new, 40, 0x3D) # write mode-appropriate code to device 40 at memory location 0x3D
                
            except KeyError:
                print('That is not a recognized mode, check IMU documentation for mode list.')

    def get_calib(self):
        '''This returns the calibration status of the IMU. It returns a tuple of (SYS status, GYR status, ACC status, MAG status).
        A value of 3 corresponds to full calibration, 0 corresponds to an uncalibrated status.'''
        
        status = self.i2c.mem_read(1, 40, 0x35) #get bytes obj of whats in CALIB_STAT register at 0x35
        mag = status[0] & 0b11 #mask to get just the mag calib status
        acc = (status[0] >> 2) & 0b11
        gyr = (status[0] >> 4) & 0b11
        sys = (status[0] >> 6) & 0b11
    
        return (sys, gyr, acc, mag)

    def get_orient(self):
        '''Returns IMU's current orientation in tuple of euler angles (x,y,z) [degrees].'''
        
        # read each axis' 2 registers and convert them from binary to an integer
        orient_data = self.i2c.mem_read(6, 40, 0x1A) #read 6 registers of x, y, z orientation data
        values = ustruct.unpack('<hhh', orient_data) #unpack the data into a tuple of 3 signed integers
        
        #each degree = 16 LSB, so we need to perfom that conversion by dividing each reading by 16
        values_final = tuple(x/16 for x in values)
        
        return values_final
    
    def get_angvel(self):
        '''This method returns the IMU's angular velocities about x, y and z axes from the gyroscope registers.
        the return format is a tuple, units are [deg/s].'''
        
        # read each axis' 2 registers and convert them from binary to an integer
        angvel_data = self.i2c.mem_read(6, 40, 0x14)
        values = ustruct.unpack('<hhh', angvel_data)

        #each degree = 16 LSB, so we need to perfom that conversion by dividing each reading by 16
        values_final = tuple(x/16 for x in values)
        
        return values_final
    
if __name__ == '__main__':
    
    imu = IMU(1)
    imu.enable()
    
    imu.mode('NDOF')
    while True:
        print('Eul Ang: ' + str(imu.get_orient()))
        print('Eul Vel: ' + str(imu.get_angvel()))
        print('Calib: ' + str(imu.get_calib()))
        utime.sleep(1) #delay 1 second
        