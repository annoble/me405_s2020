''' @file ME405mainpage.py
Brief doc for ME405mainpage.py

Detailed doc for ME405mainpage.py 

@mainpage

@section sec_intro Introduction
This html website serves as documentation for code developed in ME405: Mechatronics Design, with Prof. Charlie Refvem in spring 2020 at California Polytechnic State University.
A link to the central ME405 repository containing all source code is here: https://bitbucket.org/annoble/me405_s2020/src/master/

@section sec_mot Motor Driver
The motor driver is used to control DC motors.
A maximum of 2 motor objects can be created and used using the motordriver.Motor class which is part of the \ref motordriver package.
The motor driver and all functions of its class Motor have been tested with two gearmotor 310 RPM 6V DC micro motors.
The sourcecode for the motor driver package is here: https://bitbucket.org/annoble/me405_s2020/src/master/motordriver.py

@section sec_enc Encoder Driver
The encoder driver is used to create encoder objects which interfaces with an optical encoders on a DC motor. 
A maximum of 2 encoder objects are created and used through the encoderdriver.Encoder class which is part of the \ref encoderdriver package.
The encoder driver and all functions of its class, Encoder, have been tested using two different encoder devices on gearmotor 310 RPM 6V micro motors.
The location of the encoder driver source code is: https://bitbucket.org/annoble/me405_s2020/src/master/encoderdriver.py

@author Andrew R. Noble
@date April 24, 2020

@page tuning_page Closed Loop Step Response Tests

To tune a closed loop proportional DC motor control loop, the proportional gain term, Kp, 
was repeatedly adjusted until the desired response behavior was achieved (see below plots). 
Minimal overshoot with maximum response time were the desired response characteristics. 

Shown below is a plot illustrating the tuning process for the closed loop controller. 

@image html step_responses.JPG

I began with a gain value Kp = 1.0 which produces a small amount of overshoot and subsequent
oscillation. With each reduction in Kp gain, the overshoot was reduced, until Kp = 0.15 produced
an acceptable response: one with minimal overshoot and acceptable response time.

A video showing the motor executing a single step response test is here: https://youtu.be/zFZOcKPVxzE

@page imu_pg Inertial Measurement Unit Testing

In the development of the IMU module, a 9-axis Bosch BNO055 IMU was tested. Using a set of notecards, the origin orientation
of the IMU was found by slowly rotating the IMU until all euler angles were approximately 0. After that,
the IMU along with one of the two notecards was rotated to several different orientations, and the euler angle output was
corroborated with the physical orientation of the IMU notecard with respect to the inertial or global notecard.

A video showing the functioning IMU module implementation is here: https://www.youtube.com/watch?v=PigvyjNj-Ow&feature=youtu.be

@page proj_proposal Term Project Proposal

For a term project, I would like to create dual motor rover bot that performs a different action based on what color card is
in placed front of its on-board light sensor. For example, the rover would move in a circle for a blue card, or a square for a red card.
I envision a trike-style bot, with two front wheels driven by the motors I already have from my kit, and a single pivoting static wheel in the back that only provides stability. 

@section requirements Project Requirement Satisfaction

The encoders will both involve closed-loop control, because the difference in speed between the two motorized wheels will dictate if and how the rover turns.
The project will involve two encoders (one with each motor), which will satisfy the sensor requirement.
The project will include a small I2C ambient light detector, which will introduce a new sensor to the project.
The bot will interact with its environment by detecting the color of card it is shown (the card will likely be clipped in front of the sensor). 

@section bom Bill of Materials

Several additional items will be needed to complete this project. They are listed below.

- I2C color sensor, like this one: https://www.amazon.com/CQRobot-TCS34725FN-Raspberry-Verification-Classification/dp/B07QLL53CT
- hot-glue gun.
- small robot/toy wheels with axle diameter compatible with the 18" shafts of our motors. (gluing may be necessary).
- Cardboard or plastic plate to serve as the body of the bot.
- Pivoting castor for the third wheel of the robot. 
- 9 VDC battery to supply the motor driver
- 9 V battery clip connector wires to supply power
- AA battery to power Nucleo board
- AA battery holder with wires to hold battery
- (potentially) soldering iron
- breadboard to easily mount the color sensor

Ideally most of the items will be acquired from local hardware and dollar stores. The color sensor will need to be ordered specially, so I plan on
browsing local stores before including anything I could not find in the color sensor amazon order.

@section manuf Manufacturing and Safety

The project will be assembled easily. I plan on using primarily hot glue to mount everything. Wiring will be done without soldering at all costs
by tying wires or using the provided jumper cables assumbing they sufficiently hold the sensor wires.

During manufacturing, the only thing potentially hazardous is the solder fumes. In this case I will plan on operating the soldering 
iron in a well ventilated area. Also, I will need to be careful to avoid damaging the batteries which could spill battery acid. 

For safety I do not forsee dangers in the actual operation; the robot does not store a particularly large amount of energy, and it does
not manipulate anything that is potentially dangerous. Obviously, I will plan to keep my finders and hair away from the rotating motors.

@section timeline Timeline

The material acquisition for the project will commence as soon as the project gets approval. Ideally, this will be by Wednesday May 27.
First I will try to find as many components as I can at local stores, after which I will order parts online, probably through amazon. 

With components in-hand, I will then decide exactly how I am going to construct the robot and how I will arrange the onboard power and electronics.
I will attempt to start assembly by June. During the last week of classes, I will ideally be troubleshooting and tuning code so that my bot functions as intended.

@page final_proj Final Project Report 

For my term project, I created a rover that performs a different maneuver depending on what color card it is shown. I used the pololu balboa chassis with stablility
conversion kit, as well as 70mm wheels. Besides that, the construction was mainly made from cardboard and electrical tape. 

The project heavily utilized some past drivers written for this class, including encoderdriver.py, motordriver.py, and cl_prop_control.py.

@section sec_color Color Sensor Driver
Written for the final project, this driver allows the user to create a colorsensor object which facilitates polling of the sensor for the color it
is detecting at that time, as well as enabling and disabling the sensor. The color sensor driver required extensive calibration, and required special 
attention when the lighting conditions of the room changed. See \ref colorsensordriver package.

@section sec_simon Simon-Says Robot Driver
Written for the final project, this driver integrates the motor driver, encoder driver and color sensor driver to create a colorsimon object
that allows the user to control a robot that performs different maneuvers based on the color card it is shown. See \ref RGBsimondriver package.

Below is an image of the completed rover.

@image html simonrover.JPG

A video of the rover operating is here: https://youtu.be/fErQ7lYUdXU

@author Andrew R. Noble
@date June 12, 2020
'''
