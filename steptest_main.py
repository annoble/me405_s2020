''' @file steptest_main.py
Brief doc for steptest_main.py

Detailed doc for steptest_main.py 

@author Andrew R. Noble
@date May 10, 2020

@author Andrew R. Noble
@date May 10, 2020'''

import pyb
import utime
from encoderdriver import Encoder
from motordriver import Motor
from cl_prop_control import cl_prop_control
        
#prepare motor items and init motor
enbpin  = pyb.Pin(pyb.Pin.cpu.A10)
IN1pin  = pyb.Pin(pyb.Pin.cpu.B4)
IN2pin  = pyb.Pin(pyb.Pin.cpu.B5)
moe_tim = pyb.Timer(3, freq = 20000)
moe     = Motor(enbpin,IN1pin,IN2pin,moe_tim)
moe.enable()

# prepare encoder items and init encoder
Apin    = pyb.Pin(pyb.Pin.cpu.B6)
Bpin    = pyb.Pin(pyb.Pin.cpu.B7)
enc_tim = pyb.Timer(4, prescaler=0, period=65535)
enc     = Encoder(Apin, Bpin, enc_tim)

# prepare control loop items and init control loop
# Kp          = 10
ref_pt      = 1000
N           = 100
 
while True:
    try:
        Kp = float(input('Input the desired Kp'))
        if Kp < 0:
            print('Kp must be a positive number, try again.')
            continue
    except ValueError:
        print('Not a valid Kp, must be a positive number.')
        continue
    
    loop = cl_prop_control(Kp, ref_pt)           # make a control loop object with the entered Kp and ref_pt
    current_pos = 0                              #init current position
    positions   = []                             #init position list
    times       = []                             #init timestamp list
    
    for i in range(N+1):                         #run the step response test N times
        enc.update()                             #update recorded encoder position
        current_pos += enc.get_delta()           #add to the current position the change in encoder position
        
        positions.append(current_pos)            #store current encoder position in list
        times.append(utime.ticks_ms())           #store current time in list
        
        act_pt = loop.update(current_pos)        #have the control loop update with the current encoder position
        moe.set_duty(act_pt)                     #send the actuation value to the motor driver
        
        utime.sleep_ms(10)                       #make each loop execute about every 10 ms 
    
    moe.set_duty(5) #needed to stop the motor, moe.set_duty(0) does not work for some reason
    for n in range(N):
        print('{:}'.format(utime.ticks_diff(times[n],times[0])))
    print('break')
    for n in range(N):
        print('{:}'.format(positions[n]))
