''' @file colorsensordriver.py
Brief doc for colorsensordriver.py

Detailed doc for colorsensordriver.py 

@author Andrew R. Noble
@date June 1, 2020

@package colorsensordriver
The color sensor module implements functionality for controlling the TCS34725
Color Light-to-Digital Converter with IR Filter. The CQRobot(R) sample module
heavily influenced this module of my own. It can be found at: http://www.cqrobot.wiki/index.php/TCS34725_Color_Sensor
Note: the TCS34725 default I2C address is 41 (0x29).

@author Andrew R. Noble
@date June 1, 2020'''

import pyb
import utime
import ustruct

class ColorSensor:
    '''This class is used to interface with the TCS34725 color sensor. It allows
       the user to:
       - read the clear, red, blue, or green light data
       - enable the sensor
       - disable the sensor
       - read or write bytes of data to the sensor using I2C protocol'''
    
    # some registers and instruction code constants:
    
	## the below constants are register addresses within the TCS34725 color sensor
	
    # command register stuff:
    TCS34725_CMD_BIT        = 0b10000000 # command bit, when set, you are indicating to sensor the you are invoking the command reg
    TCS34725_CMD_Read_Byte  = 0x00       # value that is specified to read a byte
    TCS34725_CMD_Read_Word  = 0b100000   # value that is specified to read a word (used for data registers to ensure high byte and low byte are a continuous number)
    TCS34725_CMD_Clear_INT  = 0b1100110  # value used for clear channel interrupt clearing
    
    # enable register stuff
    TCS34725_ENABLE         = 0x00
    TCS34725_ENABLE_AIEN    = 0x10    # RGBC Interrupt Enable  
    TCS34725_ENABLE_WEN     = 0x08    # Wait enable - Writing 1 activates the wait timer  
    TCS34725_ENABLE_AEN     = 0x02    # RGBC Enable - Writing 1 actives the ADC, 0 disables it  
    TCS34725_ENABLE_PON     = 0x01    # Power on - Writing 1 activates the internal oscillator, 0 disables it
    
    # data registers
    TCS34725_CDATAL         = 0x14    # Clear channel data register
    TCS34725_CDATAH         = 0x15
    TCS34725_RDATAL         = 0x16    # Red channel data register
    TCS34725_RDATAH         = 0x17
    TCS34725_GDATAL         = 0x18    # Green channel data register
    TCS34725_GDATAH         = 0x19
    TCS34725_BDATAL         = 0x1A    # Blue channel data register
    TCS34725_BDATAH         = 0x1B
    
    
    def __init__(self, address=41, bus=1, debug=False):
        '''Creates and inits an I2C object on the desired nucleo bus (bus 1 = pins B6, B7)
        @param address The address parameter is the I2C address of the TCS34725 color sensor.
        @param bus The desired nucleo bus on which to run I2C communication with the color sensor.
        @param debug A simple boolean used to toggle messages displayed to the REPL that help in debugging.'''
        ## The i2c public attribute is part of the pyb module, and is used to communicate with the color sensor using inter-integrated communication (I2C) protocol.
        self.i2c = pyb.I2C(bus, pyb.I2C.MASTER)
		## The address attribute is the I2C address of the color sensor device
        self.address = address
		## The debug attribute is a boolean used to toggle debugging messages
        self.debug = debug
        if (self.debug):
          print("Reseting TCS34725")
          
    def write_byte(self, reg, value):
        '''Writes an 8-bit value to specified register.'''
        reg = reg | self.TCS34725_CMD_BIT # invoke the command bit with the provided register
        self.i2c.mem_write(value, self.address, reg) #write value (one byte), to reg in device
        if (self.debug):
            print("I2C: Write 0x%02X to register 0x%02X" % (value, reg))
        
    def read_byte(self, reg):
        '''Returns an 8-bit value from specified register.'''
        reg = reg | self.TCS34725_CMD_BIT
        result = self.i2c.mem_read(1, self.address, reg) #read one byte at specified address
        if (self.debug):
            print("I2C: Device 0x%02X returned 0x%02X from register 0x%02X" % (self.address, result[0], reg))

    def read_word(self, reg):
        '''Returns an 8-bit value from specified register.'''
        reg = reg | self.TCS34725_CMD_BIT
        result = self.i2c.mem_read(2, self.address, reg) #read 2 bytes at specified address
        if (self.debug):
            print("I2C: Device 0x%02X returned 0x%02X from register 0x%02X" % (self.address, result[0], reg))
               
    def enable(self):
        '''Enables the color sensor by setting PON (power) bit and AEN (RGB sensor) bit.'''
        self.write_byte(self.TCS34725_ENABLE, self.TCS34725_ENABLE_PON) #power on
        utime.sleep(0.01)
        self.write_byte(self.TCS34725_ENABLE, self.TCS34725_ENABLE_PON | self.TCS34725_ENABLE_AEN) #enable RGB detection
        utime.sleep(0.01)
        
    def disable(self):
        '''Disables color sensor.'''
        reg = self.read_byte(self.TCS34725_ENABLE)
        self.write_byte(self.TCS34725_ENABLE, reg & ~(self.TCS34725_ENABLE_PON | self.TCS34725_ENABLE_AEN)) # clear PON and AEN bits without modifying rest of ENABLE register
        # note: & operator in python functions as a bitmask, ~ inverts the bits and is unary
     
    def remap(self, value, maxInput, minInput, maxOutput, minOutput):
        '''Internal-use method that re-maps RBG values from their default, arbitrary scale to 0-255 scale.
		@param value The value to be re-scaled into a different range
		@param maxInput The maximum value of the input range (in this case, this is the raw RGB value produced when a color is held to the color sensor)
		@param minInput The minimum value of the input range (in this case, this is the raw RGB value produced when nothing is held to the color sensor)
		@param maxOutput The top of the range to which value is desired to be mapped, in this case, 255 is used.
		@param minOutput The bottom of the desired range, in this case, 0 is used.
        - the default readings when a red, blue and green object were shown to the camera were used to "calibrate" the values into a 0-255 range
        - this calibration process had to be done every time the lighting conditions of the environment significantly changed
        - formula: newval = (oldvalue - oldmin)*(newrange/oldrange) + newmin, where oldrange was experiementally determined at (oldmax-oldmin)
        - see here: https://stackoverflow.com/questions/929103/convert-a-number-range-to-another-range-maintaining-ratio'''
        
        value = maxInput if value > maxInput else value
        value = minInput if value < minInput else value

        inputSpan = maxInput - minInput
        outputSpan = maxOutput - minOutput

        abs_input_scaled = float(value - minInput) / float(inputSpan)

        return minOutput + (abs_input_scaled * outputSpan) 
     
    def get_RGBdata(self):
        '''Retrieves the raw RGB data from the device.'''
        reg = self.TCS34725_CDATAL | self.TCS34725_CMD_BIT
        data = self.i2c.mem_read(8, self.address, reg)
        values = ustruct.unpack('<hhhh', data)
        
        # remapping values to a 0-->255 scale (the default value spans were found using experimentation)
        
        listy = []
        listy.append(self.remap(values[0], 16000, 412, 255, 0))
        listy.append(self.remap(values[1], 1400, 462, 255, 0))
        listy.append(self.remap(values[2], 900, 336, 255, 0))
        listy.append(self.remap(values[3], 1300, 333, 255, 0))
        
        CRGB_values = tuple(x for x in listy)
        
        utime.sleep_us(10) # need to sleep so photodiode ADC integration has time to happen when this method is called repeatedly
        
        return CRGB_values
        
if __name__ == '__main__':
    
    eye = ColorSensor(debug=True)
    eye.enable()

    while True:
        print('(C,R,G,B): ' + str(eye.get_RGBdata()))
        utime.sleep(0.1)