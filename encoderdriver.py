''' @file encoderdriver.py
Brief doc for encoderdriver.py

Detailed doc for encoderdriver.py 

@author Andrew R. Noble
@date May 3, 2020

@package encoderdriver
The encoder module implements functionality for controlling a 16-bit quadrature encoder,
including updating, reading, and setting the position, as well as getting a delta value.

@author Andrew R. Noble
@date May 3, 2020'''

import pyb
import utime

class Encoder:
    '''This encoder class is used to control a quadrature encoder. It allows the user to monitor the position 
    of a motor based on its current optical encoder position as well as encoder delta, the rotation since the
    last reading
    @author Andrew R. Noble
    @date May 3, 2020'''
        
    def __init__(self, A_pin, B_pin, timer, debug=False):
        '''Creates a encoder object by initializing GPIO pins and the timer.  
        @param A_pin A pyb.Pin object to which the encoder A phase is outputted.
        @param B_pin A pyb.Pin object to which the encoder B phase is outputted.
        @param timer A pyb.Timer object to use for outputting the overall encoder count/position.'''
        
        # create instance pin, timer, and timer channel attributes
        self.A_pin        = A_pin
        self.B_pin        = B_pin
        self.timer        = timer
        self.timer_ch1    = self.timer.channel(1,pyb.Timer.ENC_A, pin=self.A_pin)
        self.timer_ch2    = self.timer.channel(2,pyb.Timer.ENC_B, pin=self.B_pin)
        self.position_old = 0 #second to last recorded encoder position
        self.position_new = 0 #last recorded position
        self.debug = debug
        if self.debug:
            print('Initializing encoder: ' + str(self))
    
    def update(self):
        '''Updates the recorded position of the encoder. it handles overflow and undeflow of the counter,
        thus the encoder position is always recorded with respect to the initial encoder position.'''
        
        delta = self.timer.counter() - self.position_new
        
        if delta > 65535/2: #check for underflow
            delta -= 65535 #fix by subtracting 65535
            
        elif delta < -65535/2: #check for overflow
            delta += 65535 #fix by adding 65535
            
        else: #neither underflow nor overflow happened, delta is valid
            pass
        
        self.position_old = self.position_new #advance the position record
        self.position_new  = self.position_old + delta
        
        if self.debug:
            print('Updating encoder: ' + str(self))
        
    def get_position(self):
        '''Returns the most recent position of the encoder (the one last
        recorded using update()).'''
        
        #print('The current encoder position is {:}'.format(self.position_new))
        if self.debug:
            print('Getting position of encoder: ' + str(self))
        return self.position_new
    
    def set_position(self, setval):
        '''Resets the encoder count/position to the specified value.
        @param setval A positive integer between 0 and 65,535 to which
                      the encoder will be set.'''
                      
        self.position_old = setval #reset the two stored positions to the setval 
        self.position_new = setval
        self.timer.counter(setval) #set current encoder count to setval
        if self.debug:
            print('Setting the encoder value to {:}'.format(setval))
    
    def get_delta(self):
        '''Returns the difference in recorded position between the two
        most recent calls to update().'''
        
        delta = self.position_new - self.position_old
        
        if self.debug:
            print('Getting delta of encoder: ' + str(self))
            
        return delta
        
if __name__ == '__main__':
    '''This block of code is a test case for the encoder object.'''
   
    # Creating the pin objects used for interfacing with the encoder.
    Apin = pyb.Pin(pyb.Pin.cpu.B6)
    Bpin = pyb.Pin(pyb.Pin.cpu.B7)
   
    # Creating the timer object used to output the encoder count to
    tim = pyb.Timer(4, prescaler=0, period=65535)
   
    # Creating a encoder object and passing in the desired pins and timer
    enc1 = Encoder(Apin,Bpin,tim)
    
    # create a second encoder object
    Apin = pyb.Pin(pyb.Pin.cpu.C6)
    Bpin = pyb.Pin(pyb.Pin.cpu.C7)
    tim = pyb.Timer(8, prescaler=0, period=65535)
    enc2 = Encoder(Apin,Bpin,tim)
    
    while True:
        enc1.update()
        print(enc1.get_position())
        utime.sleep_ms(100) #this makes the encoder run a whole lot better
        