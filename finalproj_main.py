''' @file finalproj_main.py
Brief doc for finalproj_main.py

This main script mobilizes all previously written modules to run a color-sensing simon says robot.

@author Andrew R. Noble
@date June 1, 2020

@author Andrew R. Noble
@date June 1, 2020'''

import pyb
import utime
from cl_prop_control import cl_prop_control
from motordriver import Motor
from encoderdriver import Encoder
from colorsensordriver import ColorSensor
from RGBsimondriver import RGBsimon

##########
# initiation: instantiate and enable 2 motor objects, 2 encoder objects, a colorsensor object and a proportional control loop object.
##########

# right motor
## The public attribute Pin s part of the pyb module and alows control of pins on the nucleo board.
enbpin_R  = pyb.Pin(pyb.Pin.cpu.A10) #receptacle is female
IN1pin_R  = pyb.Pin(pyb.Pin.cpu.B4) # f
IN2pin_R  = pyb.Pin(pyb.Pin.cpu.B5) # f
moe_tim_R = pyb.Timer(3, freq = 20000)
moe_R     = Motor(enbpin_R, IN1pin_R, IN2pin_R, moe_tim_R)
moe_R.enable()

# left motor
enbpin_L  = pyb.Pin(pyb.Pin.cpu.C1) # m
IN1pin_L  = pyb.Pin(pyb.Pin.cpu.A0) # m
IN2pin_L  = pyb.Pin(pyb.Pin.cpu.A1) # m
moe_tim_L = pyb.Timer(5, freq = 20000)
moe_L     = Motor(enbpin_L, IN1pin_L, IN2pin_L, moe_tim_L)
moe_L.enable()

# right encoder
Apin_R    = pyb.Pin(pyb.Pin.cpu.B6) # f
Bpin_R    = pyb.Pin(pyb.Pin.cpu.B7) # m
enc_tim_R = pyb.Timer(4, prescaler=0, period=65535)
enc_R     = Encoder(Apin_R, Bpin_R, enc_tim_R)

# left encoder
Apin_L    = pyb.Pin(pyb.Pin.cpu.C6) # m
Bpin_L    = pyb.Pin(pyb.Pin.cpu.C7) # m
enc_tim_L = pyb.Timer(8, prescaler=0, period=65535)
enc_L     = Encoder(Apin_L, Bpin_L, enc_tim_L)

# color sensor
eye = ColorSensor()
eye.enable()

# control loop (both motors use this to regulate speed)

loop = cl_prop_control(5) #Kp

# create RGBsimon object

simon = RGBsimon(moe_R, moe_L, enc_R, enc_L, eye, loop, debug = True)

########
# Action loop: simon will look for a color, then perform the correct action, and repeat.
# note: about 2,000 encoder counts per motor revolution
########
 
for i in range(10):
    while True:
        color = simon.get_color()
        print(color)
        utime.sleep(0.01)
        if color == 'diffuse':
            continue
        else:
            break
        
    if color == 'red':
        simon.cw_circle()        
    elif color == 'green':
        simon.line()        
    elif color == 'blue':
        simon.ccw_circle()

simon.moe_r.disable()
simon.moe_l.disable()

