''' @file cl_prop_control.py
Brief doc for cl_prop_control.py

Detailed doc for cl_prop_control.py 

@author Andrew R. Noble
@date May 10, 2020

@package cl_prop_control This module contains a class, cl_prop_control, that executes closed-loop proportional control using
a proportional gain constant, Kp, reference value, ref_pt, and a current reading, meas_pt.

@author Andrew R. Noble
@date May 10, 2020'''

class cl_prop_control:
    '''This class performs closed-loop proportional control using a gain value, Kp
    and a reference or set point.'''

    def __init__(self, Kp):
        '''This function creates a proportional controller object.
        @param Kp The loop's proportional control gain value. Units = % duty cycle/encoder count.'''
        self.Kp = Kp
                
    def update(self, meas_pt, ref_pt):
        '''This method accepts a reference value and a measured value, then returns an appropriate actuation value for the system
            in order for the system's measured/actual value to reach the reference value.
        @param meas_pt A measured numerical value that represents the current operating condition of the system. Units = encoder counts.
        @param ref_pt  A numerical value representing the desired operating point of the system. Units = encoder counts.'''

        act_pt = self.Kp*(meas_pt - ref_pt) # produce the actuation value by multiplying the error signal by the control gain
        
        if act_pt > 100: # need to saturate the duty cycle necause set_duty only accepts between -100 and 100
            act_pt = 100
            
        elif act_pt < -100:
            act_pt = -100
        
        return act_pt