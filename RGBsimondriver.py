''' @file RGBsimondriver.py
Brief doc for RGBsimondriver.py

Detailed doc for RGBsimondriver.py 

@author Andrew R. Noble
@date June 1, 2020

@package RGBsimondriver
The RGB simon module is used to drive a two-wheeled robot that performs ddifferent actions based on what color card it is shown.
It is reliant on several other modules to drive the encoders, motors and color sensor that are onboard the robot. Additionally, it uses
a closed-loop proportional control module to control the position of the motors.

@author Andrew R. Noble
@date June 1, 2020'''

import pyb
import utime
from cl_prop_control import cl_prop_control
from motordriver import Motor
from encoderdriver import Encoder
from colorsensordriver import ColorSensor

class RGBsimon:
    '''This class is used to create and inerface with the simon says robot object. It can be used to:
    - retrieve the detected color on the onboard color sensor
    - instruct the robot to maneuver in a line, clockwise circle, or counter-clockwise circle.'''
    
    def __init__(self, motorR, motorL, encoderR, encoderL, colorsensor, control_loop, debug=False):
        '''Initializes the color simon bot by creating a colorsimon instance given 2 motor objects, 2 encoder objects, a color sensor object and a control loop.'''
        ## moe_r is a motor object that interfaces with the gear motor on the right side of the rover
        self.moe_r = motorR
		## moe_l is a motor object corresponding to the rover's left motor
        self.moe_l = motorL
		## enc_r the right motor's encoder object
        self.enc_r = encoderR
		## enc_l the left motor's encoder object
        self.enc_l = encoderL
		## eye is the color sensor object that interfaces with the onboard color sensor
        self.eye   = colorsensor
		## loop is a closed-loop proportional control object used to control the position of the motors (and thus control the movement of the rover)
        self.loop  = control_loop
		## debug is a boolean used to toggle debugging messages
        self.debug = debug
        if self.debug:
            print('Creating a colorsimon object.')
            
    def get_color(self):
        '''Polls the onboard color sensor to determine what color is being shown to the color sensor.
		@return color The closest-guess color is the value returned by this function, its accuracy is dependent on calibration present in the \ref colorsensordriver package'''
        colors = self.eye.get_RGBdata() # get_RGBdata returns a tuple of colors (C, R, G, B) (don't use C), each on 0-->255 scale
        
        R = colors[1]
        G = colors[2]
        B = colors[3]
        
        if R > 160 and R > G and R > B:
            if self.debug:
                print('Bot says: red detected!')
            return 'red'
        
        elif G > 160 and G > R and G > B:
            if self.debug:
                print('Bot says: green detected!')
            return 'green'
        
        elif B > 160 and B > R and B > G:
            if self.debug:
                print('Bot says: blue detected!')
            return 'blue'
        
        else:
#             if self.debug:
#                 print('Bot says: no distinct color detected!')
            return 'diffuse'
            
    def line(self):
        '''Instructs the bot to move back and forth in a line.'''
        
        if self.debug:
            print('Instruction given: move in a line!')
            
        while True: #instruct tobot to move forward 8000 encoder counts (4 revolutions of wheel)
            self.enc_r.update()
            self.enc_l.update()
            r_pos = self.enc_r.get_position()
            l_pos = self.enc_l.get_position()
            act_pt_r = self.loop.update(r_pos, -8000)
            act_pt_l = self.loop.update(l_pos, 8000)
            self.moe_r.set_duty(act_pt_r)
            self.moe_l.set_duty(act_pt_l)
            if r_pos < -8000 or l_pos > 8000:
                break
            
        while True: #instruct tobot to move backward 8000 encoder counts (4 revolutions of wheel)
            self.enc_r.update()
            self.enc_l.update()
            r_pos = self.enc_r.get_position()
            l_pos = self.enc_l.get_position()
            act_pt_r = self.loop.update(r_pos, 0)
            act_pt_l = self.loop.update(l_pos, 0)
            self.moe_r.set_duty(act_pt_r)
            self.moe_l.set_duty(act_pt_l)
            if r_pos > 0 and l_pos < 0:
                self.enc_r.set_position(0)
                self.enc_l.set_position(0)
                break
            
        self.moe_r.set_duty(5)
        self.moe_l.set_duty(5)
        
    def ccw_circle(self):
        '''Instructs the robot to move in a ccw circle.'''
        
        if self.debug:
            print('Instruction given: drive in a counter clockwise circle!')
        
        while True:
            self.enc_r.update()
            self.enc_l.update()
            r_pos = self.enc_r.get_position()
            l_pos = self.enc_l.get_position()
            act_pt_r = self.loop.update(r_pos, -8000)
            act_pt_l = self.loop.update(l_pos, -8000)
            self.moe_r.set_duty(act_pt_r)
            self.moe_l.set_duty(act_pt_l)
            if r_pos < -8000 and l_pos < -8000:
                self.enc_r.set_position(0)
                self.enc_l.set_position(0)
                break
            
        self.moe_r.set_duty(5)
        self.moe_l.set_duty(5)
        
    def cw_circle(self):
        '''Instructs the robot to move in a cw circle.'''
        
        if self.debug:
            print('Instruction given: drive in a clockwise circle!')
        
        while True:
            self.enc_r.update()
            self.enc_l.update()
            r_pos = self.enc_r.get_position()
            l_pos = self.enc_l.get_position()
            act_pt_r = self.loop.update(r_pos, 8000)
            act_pt_l = self.loop.update(l_pos, 8000)
            self.moe_r.set_duty(act_pt_r)
            self.moe_l.set_duty(act_pt_l)
            if r_pos > 8000 and l_pos > 8000:
                self.enc_r.set_position(0)
                self.enc_l.set_position(0)
                break
        
        self.moe_r.set_duty(5)
        self.moe_l.set_duty(5)